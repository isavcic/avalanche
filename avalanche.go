package main

import (
	"log"
	"log/syslog"
	"os"
	"os/exec"
	"os/signal"
	"syscall"
	"time"

	ps "github.com/mitchellh/go-ps"
)

func main() {

	// Set up logging to syslog
	w, err := syslog.New(syslog.LOG_NOTICE, "avalanche")
	if err == nil {
		log.SetOutput(w)
	}
	log.SetFlags(0)

	// Ignore all signals
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan)
	go func() {
		for _ = range sigChan {
		}
	}()

	// Take the cmd to start from the second argument, pass the other arguments as arguments to it
	args := os.Args
	cmd := exec.Command(args[1], args[2:]...)
	cmd.SysProcAttr = &syscall.SysProcAttr{
		Setpgid:   true,
		Pdeathsig: syscall.SIGKILL,
	}

	// Pass the stdout and stderr from the cmd to stdout and stderr
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	err = cmd.Start()
	if err != nil {
		log.Fatalln("cmd.Start() failed with:", err)
	}

	// Discover the parent process
	parent, _ := ps.FindProcess(os.Getppid())

	// Monitor for the change of the parent process
	go monitorPPID(parent, cmd)

	// Wait for the command to finish
	err = cmd.Wait()
	if err != nil {
		log.Println("cmd.Wait() failed with:", err)
	}
}

func monitorPPID(parent ps.Process, cmd *exec.Cmd) {
	// We have to look up the parent of the parent:
	// sshd -> sudo -> avalanche -> ...
	pppid := parent.PPid()

	log.Println("PID of parent's parent:", pppid)
	log.Println("PID of cmd:", cmd.Process.Pid)
	for {
		parent, _ = ps.FindProcess(os.Getppid())
		if pppid != parent.PPid() {
			log.Println("Parent process' parent changed to", parent.PPid())
			log.Println("Killing children of", cmd.Process.Pid)
			err := syscall.Kill(-cmd.Process.Pid, syscall.SIGKILL)
			os.Exit(0)
			if err != nil {
				log.Fatalln("Failed to kill process! Suiciding")
			}
		} else {
			time.Sleep(1 * time.Second)
		}
	}
}
